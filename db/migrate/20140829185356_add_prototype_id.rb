class AddPrototypeId < ActiveRecord::Migration
  def change
    add_column :noids, :prototype_id, :integer, :default => nil
  end
end
