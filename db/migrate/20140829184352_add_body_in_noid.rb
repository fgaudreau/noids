class AddBodyInNoid < ActiveRecord::Migration
  def change
    add_column :noids, :body, :text, :default => nil
  end
end
