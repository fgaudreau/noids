class CreateNoids < ActiveRecord::Migration
  def change
    create_table :noids do |t|
      t.string :name

      t.timestamps
    end
  end
end
