require 'test_helper'

class NoidsControllerTest < ActionController::TestCase
  setup do
    @noid = noids(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:noids)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create noid" do
    assert_difference('Noid.count') do
      post :create, noid: { name: @noid.name }
    end

    assert_redirected_to noid_path(assigns(:noid))
  end

  test "should show noid" do
    get :show, id: @noid
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @noid
    assert_response :success
  end

  test "should update noid" do
    patch :update, id: @noid, noid: { name: @noid.name }
    assert_redirected_to noid_path(assigns(:noid))
  end

  test "should destroy noid" do
    assert_difference('Noid.count', -1) do
      delete :destroy, id: @noid
    end

    assert_redirected_to noids_path
  end
end
