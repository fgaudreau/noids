class Noid < ActiveRecord::Base
      belongs_to :prototype, :class_name => 'Noid', :foreign_key => "prototype_id"
      has_many   :instances, :class_name => 'Noid', :foreign_key => "prototype_id"
end


