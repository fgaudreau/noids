class NoidsController < ApplicationController
  before_action :set_noid, only: [:show, :edit, :update, :destroy]

  # GET /noids
  # GET /noids.json
  def index
    @noids = Noid.all
  end

  # GET /noids/1
  # GET /noids/1.json
  def show
  end

  # GET /noids/new
  def new
    @noid = Noid.new
  end

  # GET /noids/1/edit
  def edit
  end

  # POST /noids
  # POST /noids.json
  def create
    @noid = Noid.new(noid_params)

    respond_to do |format|
      if @noid.save
        format.html { redirect_to @noid, notice: 'Noid was successfully created.' }
        format.json { render :show, status: :created, location: @noid }
      else
        format.html { render :new }
        format.json { render json: @noid.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /noids/1
  # PATCH/PUT /noids/1.json
  def update
    respond_to do |format|
      if @noid.update(noid_params)
        format.html { redirect_to @noid, notice: 'Noid was successfully updated.' }
        format.json { render :show, status: :ok, location: @noid }
      else
        format.html { render :edit }
        format.json { render json: @noid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /noids/1
  # DELETE /noids/1.json
  def destroy
    @noid.destroy
    respond_to do |format|
      format.html { redirect_to noids_url, notice: 'Noid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_noid
      @noid = Noid.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def noid_params
      params.require(:noid).permit(:name)
    end
end
