json.array!(@noids) do |noid|
  json.extract! noid, :id, :name
  json.url noid_url(noid, format: :json)
end
